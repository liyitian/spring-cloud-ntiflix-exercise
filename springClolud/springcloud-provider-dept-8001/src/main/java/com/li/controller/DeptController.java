package com.li.controller;


import com.li.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.li.service.DeptSerivce;

import java.util.List;

//提供Restful服务
@RestController
public class DeptController {
    @Autowired
    private DeptSerivce deptService;
    @Autowired
    private DiscoveryClient discoveryClient; //获取一些配置信息，得到具体的微服务

    @PostMapping("/dept/add")
    public boolean addDept(Dept dept){
        return deptService.addDept(dept);
    }
    @GetMapping("/dept/get/{id}")
    public Dept queryByid(@PathVariable long id){
        return deptService.queryByid(id);
    }
    @GetMapping("/dept/list")
    public List<Dept> queryAll(){
        return deptService.queryAll();
    }
    //注册进来的微服务，获取一些消息 合作开发时使用的
    @GetMapping("/dept/discovery")
    public Object Discovery(){
        //获得微服务列表的清单
        List<String> services = discoveryClient.getServices();
        System.out.println("discovery=>services"+services);

        //得到具体的微服务信息,通过具体的微服务ID，也就是applicationName
        List<ServiceInstance> instances = discoveryClient.getInstances("SPRINGCLOUD-PROVIDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.out.println(instance.getHost()
                    +"\t"+instance.getServiceId()
                    +"\t"+instance.getPort()
                    +"\t"+instance.getUri());
        }
        return this.discoveryClient;
    }
}

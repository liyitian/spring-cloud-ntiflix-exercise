package com.li.service;

import com.li.springcloud.pojo.Dept;

import java.util.List;

public interface DeptSerivce {
    Boolean addDept(Dept dept);

    Dept queryByid(long id);

    List queryAll();
}

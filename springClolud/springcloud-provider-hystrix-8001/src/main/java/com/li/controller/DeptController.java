package com.li.controller;


import com.li.service.DeptSerivce;
import com.li.springcloud.pojo.Dept;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//提供Restful服务
@RestController
public class DeptController {
    @Autowired
    private DeptSerivce deptSerivce;

    @GetMapping("/dept/get/{id}")
    @HystrixCommand(fallbackMethod = "hystrixGet")
    public Dept get(@PathVariable("id") long id) {
        Dept dept = deptSerivce.queryByid(id);
        if (dept == null) {
            throw new RuntimeException("id=>" + id + "，不存在改用户，或者信息找不到");
        }
        return dept;
    }

    //备选方案
    public Dept hystrixGet(@PathVariable("id") long id) {
        return new Dept()
                .setDepton(id)
                .setDname("id=>" + id + "，不存在改用户，或者信息找不到，null---@Hystrix")
                .setDb_source("no this database in MySQL");
    }

    @GetMapping("/dept/list")
    public List<Dept> queryAll(){
        return deptSerivce.queryAll();
    }
}

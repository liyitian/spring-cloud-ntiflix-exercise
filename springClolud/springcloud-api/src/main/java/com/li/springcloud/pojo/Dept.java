package com.li.springcloud.pojo;

import lombok.*;
import lombok.experimental.Accessors;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.Serializable;
@NoArgsConstructor
@AllArgsConstructor
public class Dept implements Serializable {
    private Long depton;
    private String dname;
    //一个服务的信息可能来自不同数据库
    private String db_source;

    public Long getDepton() {
        return depton;
    }

    public Dept setDepton(Long depton) {
        this.depton = depton;
        return this;
    }

    public String getDname() {
        return dname;
    }

    public Dept setDname(String dname) {
        this.dname = dname;
        return this;
    }

    public String getDb_source() {
        return db_source;
    }

    public Dept setDb_source(String db_source) {
        this.db_source = db_source;
        return this;
    }

    public Dept(String dname) {
        this.dname = dname;
    }

}
package com.li.springcloud.serivce;

import com.li.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
//服务降级
@Component
public class DeptServiceFallbackFactory implements FallbackFactory {
    @Override
    public DeptClientSerivce create(Throwable throwable) {
        return new DeptClientSerivce() {
            @Override
            public Dept queryById(long id) {
                return new Dept()
                        .setDname("用户"+id+"改服务已被暂时关闭，暂被另一个服务使用资源")
                        .setDb_source("")
                        .setDepton(id);
            }

            @Override
            public List<Dept> queryAll() {
                return null;
            }

            @Override
            public boolean addDept(Dept dept) {
                return false;
            }
        };
    }
}

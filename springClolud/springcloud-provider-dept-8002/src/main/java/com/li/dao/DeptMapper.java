package com.li.dao;

import com.li.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptMapper {
    Boolean addDept(Dept dept);

    Dept queryByid(long id);

    List queryAll();
}

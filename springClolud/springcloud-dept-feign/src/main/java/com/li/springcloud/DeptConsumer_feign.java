package com.li.springcloud;

import com.li.myiruble.LiRuble;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

//Ribbon 与 Eureka 整合后，客户端可以直接调用服务，不需要关心服务地址
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})//DataSourceAutoConfiguration.class阻止Spring boot自动注入dataSource
@EnableEurekaClient
@EnableFeignClients(basePackages={"com.li.springcloud"})
public class DeptConsumer_feign {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_feign.class,args);
    }
}

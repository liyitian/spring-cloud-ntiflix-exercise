package com.li.springcloud.controller;

import com.li.springcloud.pojo.Dept;
import com.li.springcloud.serivce.DeptClientSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class DeptConsumerController {
    @Autowired
    DeptClientSerivce deptClientSerivce = null;
    @RequestMapping("/consumer/dept/add")
    public boolean add(Dept dept){
        return this.deptClientSerivce.addDept(dept);
    }

    @RequestMapping("/consumer/dept/get/{id}")
    public Dept getDept(@PathVariable Long id){
        return deptClientSerivce.queryById(id) ;
    }

    @RequestMapping("/consumer/dept/list")
    @ResponseBody
    public List<Dept> list(){
        return deptClientSerivce.queryAll();
    }
}

package com.li.service.impl;

import com.li.dao.DeptMapper;
import com.li.service.DeptSerivce;
import com.li.springcloud.pojo.Dept;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

@Service
public class DeptSerivceImpl implements DeptSerivce {
    @Resource
    DeptMapper deptDao;

    @Override
    public Boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryByid(long id) {
        return deptDao.queryByid(id);
    }

    @Override
    public List queryAll() {
        return deptDao.queryAll();
    }
}

package com.li.springcloud.controller;

import com.li.springcloud.pojo.Dept;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class DeptConsumerController {
    //理解：消费者，不应该有service层
    //RestTemplate ...供我们调用
    @Resource
    private RestTemplate restTemplate; //提供多种便捷访问服务的方法，简单的restful服务模板

    //通过Ribbon进行负载均衡，这里的地址应该是一个变量，这个变量就是服务注册中心的服务的ID，通过服务ID来访问
//    private static final String REST_URL_PREFIX = "http://localhost:8001/"; //提供者
    private static final String REST_URL_PREFIX = "http://SPRINGCLOUD-PROVIDER-DEPT/"; //提供者

    @RequestMapping("/consumer/dept/add")
    public boolean add(Dept dept){
        return restTemplate.postForObject(REST_URL_PREFIX+"dept/add",dept,Boolean.class);
    }

    @RequestMapping("/consumer/dept/get/{id}")
    public Dept deptAdd(@PathVariable long id){
        return restTemplate.getForObject(REST_URL_PREFIX+"dept/get/"+id,Dept.class) ;
    }

    @RequestMapping("/consumer/dept/list")
    @ResponseBody
    public List<Dept> list(){
        return restTemplate.getForObject(REST_URL_PREFIX+"dept/list",List.class);
    }
}

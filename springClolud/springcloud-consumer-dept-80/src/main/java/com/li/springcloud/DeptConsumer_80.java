package com.li.springcloud;

import com.li.myiruble.LiRuble;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

//Ribbon 与 Eureka 整合后，客户端可以直接调用服务，不需要关心服务地址
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})//DataSourceAutoConfiguration.class阻止Spring boot自动注入dataSource
@EnableEurekaClient
@RibbonClient(name = "SPRINGCLOUD-PROVIDER-DEPT",configuration = LiRuble.class) //扫描自定义ribbon
public class DeptConsumer_80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_80.class,args);
    }
}

package com.li.springcloud.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigBean {
    //配置负载均衡失效RestTemplate
    //IRule
    //AvailabilityFilteringRule：先过滤掉，跳闸或服务故障的服务-对剩下的进行轮询
    //RoundRobinRule： 轮询 轮流访问服务
    //RandomRule ： 随机
    //RetryRule ：会先按照轮询获取服务，如果服务获取失败，则会在指定的时间进行 重试
    @Bean
    @LoadBalanced //Ribbon
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}

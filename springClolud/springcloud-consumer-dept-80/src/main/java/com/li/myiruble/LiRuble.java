package com.li.myiruble;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LiRuble {
    @Bean
    public IRule myIRule(){
        return new LiRandomRule(); //默认是轮询，现在是自定义
    }
}
